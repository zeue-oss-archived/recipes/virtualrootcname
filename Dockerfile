FROM node:8.11

WORKDIR /usr/src/app

COPY . /usr/src/app/

CMD [ "npm", "start"]