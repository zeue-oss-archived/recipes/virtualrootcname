var http = require("http");

http
  .createServer((req, res) => {
    let redirectUrl = "www." + req.headers.host;
    console.log("redirecting " + req.headers.host + " to " + redirectUrl)
    res.writeHead(302, { Location: "http://" + redirectUrl });
    res.end();
  })
  .listen(process.env.PORT || "3000");
